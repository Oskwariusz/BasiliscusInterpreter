// Basiliscus beta
// Author: Jakub Oskwarek
// Printing to the console
using System;
// StreamReader to read Code from a file
using System.IO;
// Counting characters in the Code string
using System.Linq; 
// Regex to check if the file is a .blcs file
using System.Text.RegularExpressions;
// List<byte> as program data
using System.Collections.Generic;

namespace Basiliscus
{
	/// <summary>This program executes Basiliscus Code consumed from a .blcs file</summary>
	/// <permission>This class is accessible from outside the namespace</permission>
	public static class Program
	{
		/// <remarks>Program entry point</remarks>
		/// <permission>This method can be accessed from everywhere</permission>
		public static void Main(string[] args)
		{
			// If there are no arguments, call BasiliscusInterpreter.Info static method
			if(args.Length<1)
			{
				BasiliscusInterpreter.Info();
			}
			// Else create a new BasiliscusInterpreter
			else
			{
				BasiliscusInterpreter interpreter = new BasiliscusInterpreter(args[0]);
				interpreter.ExecuteCode();
			}
		}
	}
	
	/// <summary>This class handles the interpreting</summary>
	public class BasiliscusInterpreter
	{
		// Some symbol constants
		/// <remarks>Moving pointer to the left</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char MOVE_POINTER_LEFT = '<';
		/// <remarks>Moving pointer to the right</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char MOVE_POINTER_RIGHT = '>';
		/// <remarks>Moving pointer to the start</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char MOVE_POINTER_TO_START = '*';
		/// <remarks>Moving pointer to the end</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char MOVE_POINTER_TO_END = '=';
		/// <remarks>Data cell incrementation</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char DATA_CELL_INCREMENT = '+';
		/// <remarks>Data cell decrementation</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char DATA_CELL_DECREMENT = '-';
		/// <remarks>Loop opening bracket</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char LOOP_OPENING = '[';
		/// <remarks>Loop closing bracket</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char LOOP_CLOSING = ']';
		/// <remarks>Printing as Unicode character</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char OUTPUT_AS_UNICODE = '@';
		/// <remarks>Printing as number</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char OUTPUT_AS_NUMBER = '#';
		/// <remarks>Program termination</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char TERMINATE = '.';
		/// <remarks>Shown input</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char INPUT_SHOW = ';';
		/// <remarks>Hidden input</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char INPUT_HIDE = ':';
		/// <remarks>Adding a new cell to the left</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char ADD_CELL_LEFT = '{';
		/// <remarks>Adding a new cell to the right</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		protected const char ADD_CELL_RIGHT = '}';

		/// <remarks>Contains symbols to match (as keys) and their matching symbols (as values)</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		/// <permission>readonly to prevent accidental changes while executing the Code</permission>
		/// <example>MatchingSymbols['['] => ']' -> ']' matches '['</example>
		/// <example>MatchingSymbols[']'] => '[' -> '[' matches ']'</example>
		protected static readonly Dictionary<char, char> MatchingSymbols = new Dictionary<char, char>()
		{
			{LOOP_OPENING, LOOP_CLOSING},
			{LOOP_CLOSING, LOOP_OPENING}
		};
		/// <remarks>Contains the values that represent the direction of search for matching symbols</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		/// <permission>readonly to prevent accidental changes while executing the Code</permission>
		/// <example>1 -> go to the right in the Code and look for a matching symbol -> ']' should be to the right from its matching '['</example>
		/// <example>-1 -> go to the left in the Code and look for a matching symbol -> '[' should be to the left from its matching ']'</example>
		protected static readonly Dictionary<char, int> SymbolSearchDirectionValues = new Dictionary<char, int>()
		{
			{LOOP_OPENING, 1},
			{LOOP_CLOSING, -1}
		};
		/// <remarks>Contains symbols that should be treated as the opening ones (in a pair)</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		/// <permission>readonly to prevent accidental changes while executing the Code</permission>
		protected static readonly char[] Opening = new char[] {'['};
		/// <remarks>Contains symbols that should be treated as the closing ones (in a pair)</remarks>
		/// <permission>protected to be accessed directly in potential child classes</permission>
		/// <permission>readonly to prevent accidental changes while executing the Code</permission>
		protected static readonly char[] Closing = new char[] {']'};
		/// <remarks>Program Code loaded from a file</remarks>
		/// <permission>readonly to prevent accidental changes while executing it</permission>
		protected readonly string Code;
		/// <remarks>Current Data cell index</remarks>
		protected int Pointer;
		/// <remarks>Current instruction symbol index</remarks>
		protected int CodeIndex;
		/// <remarks>Flag that tells the interpreter if the program is still running</remarks>
        protected bool Exit;
		/// <remarks>Program data as a List of bytes</remarks>
		protected List<byte> Data;
		
		/// <remarks>Public constructor that takes the source file path and initializes each field</remarks>
		/// <permission>The constructor is public and can be called from everywhere</permission>
		/// <param name="path">Path to the source file</param>
		public BasiliscusInterpreter(string path)
		{
			// By default it is possible to run the program
			Exit = false;
			// This line is trying to load Code from a .blcs file
			Code = LoadCodeFromFile(path);
			// Commands will be executed from left to right so the instruction index is being located at the first symbol
			CodeIndex = 0;
			// Data is being initialized and has one cell set to 0
			Data = new List<byte>() {0};
			// Pointer is being located at the first cell on the left
			Pointer = 0;
			// If there are any errors, this List<string> contains some information about them, else it is null
			List<string> errors = CheckForErrors();
			if(errors!=null)
			{
				// If there are any errors, the method that prints to the console some information about them is being called
				ShowErrorMessages(errors);
				// The Code is being set to the termination sequence
				Code = ".";
			}
		}
		
		/// <remarks>Tries to load Code from a specified .blcs file. If a problem occurs, prints error message to the console</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		/// <param>Path to the source file</param>
		/// <returns>The loaded code or the termination sequence if there are any problems</returns>
		protected string LoadCodeFromFile(string filePath)
		{
			// The Code string, by default set to the termination sequence
			string code = ".";
			// Tests if the specified file is a .blcs file
			Regex extensionTester = new Regex(@"\.blcs$");
			// If it is, it tries to load Code from this file
			if(extensionTester.IsMatch(filePath))
			{
				// by using a StreamReader object
				StreamReader sr = null;
				try
				{
					// Tries to create a new StreamReader using the filePath
					sr = new StreamReader(filePath);
					// It reads all the Code at once
					code = sr.ReadToEnd();
				}
				catch(IOException)
				{
					// If there is a problem while opening the file, it prints a message about that to the console
					Console.WriteLine("Basiliscus: Failed to open file.");
				}
				finally
				{
					// Finally, it closes the file
					if(sr!=null)
					{
						sr.Close();
						sr.Dispose();
					}
				}
			}
			// If it is not a .blcs file it prints the error message to the console
			else 
			{
                Console.WriteLine("Basiliscus: A .blcs file needed.");
			}
			return code;
		}
		
		/// <remarks>Checks if there are any errors in the Code</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		/// <returns>A List of errors locations as strings or null if there are no errors</returns>
		protected List<string> CheckForErrors()
		{
			// Contains the current Code line number
			uint line = 1;
			// Contains the current Code column number
			uint column = 1;
			// Stack contains the opening symbols that must be matched
			Stack<Symbol> toMatch = new Stack<Symbol>();
			// Contains the errors locations
			List<string> unmatched = new List<string>();
			// This loop checks every instruction symbol (as c) in the Code
			foreach(char c in Code)
			{
				// If c is a new line symbol, column line is being incremented and column is being set to default
				if(c=='\n')
				{
					line++;
					column = 0;
				}
				// If c is an opening symbol, it's being pushed to the stack with its loaction
				if(Opening.Contains(c))
				{
					toMatch.Push(new Symbol(line, column, c));
				}
				else if(Closing.Contains(c))
				{
					if(toMatch.Count>0 && c==MatchingSymbols[toMatch.Peek().Character])
					{
						/*
							If c is a closing Symbol and there are any opening symbols in the stack and c matches the last opening symbol
							in the stack, this symbol is in the right place and its matching opening symbol is being popped from the stack
						*/
						toMatch.Pop();
					}
					else
					{
						// Else it is an error so its location is being added to the list
						unmatched.Add($"(line {line}, column {column}) '{c}'");
					}
				}
				// Moving to the next column
				column++;
			}
			// The stack is being reversed so that the unmatched opening symbols are added to the errors list in the correct order
			toMatch.Reverse();
			while(toMatch.Count>0)
			{
				Symbol s = toMatch.Pop();
				unmatched.Add($"(line {s.Line}, column {s.Column}) '{s.Character}'");
			}
			// If there are finally any errors, the list with their locations is being returned
			if(unmatched.Count>0)
			{
				return unmatched;
			}
			// Else null is being returned
			return null;
		}

		/// <remarks>Prints all error messages to the console</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		/// <param name="errors">The list of errors</param>
		protected void ShowErrorMessages(List<string> errors)
		{
			foreach(string error in errors)
			{
				Console.WriteLine("Invalid syntax: " + error + " is unmatched");
			}
			// Waits for input
			Console.ReadKey(true);
		}
		
		/// <remarks>Displays some information about the interpreter version and about the language commands</remarks>
		/// <permission>public and can be called from everywhere</permission>
		public static void Info()
		{
			Console.WriteLine("Basiliscus beta");
			Console.WriteLine("Author: Jakub Oskwarek");
			Console.ReadKey(true);
		}
		
		/// <remarks>The actual interpreter</remarks>
		/// <permission>public and can be called from everywhere</permission>
		public void ExecuteCode()
		{
			// The loop continues interpreting and executing the Code while the end of Code isn't reached and Exit equals false
			while(CodeIndex<Code.Length && !Exit)
			{
				// Interpreter action depends on the current Code character
                switch(Code[CodeIndex])
                {
                    case MOVE_POINTER_RIGHT:
						// Moves the DataPointer one cell to the right
                        PointerIncrement();
                        break;
                    case MOVE_POINTER_LEFT:
						// Moves the DataPointer one cell to the left
                        PointerDecrement();
                        break;
					case MOVE_POINTER_TO_START:
						// Moves the DataPointer to the beginning of the Data List
						PointerToStart();
						break;
					case MOVE_POINTER_TO_END:
						// Moves the DataPointer to the end of the Data List
						PointerToEnd();
						break;
					case DATA_CELL_INCREMENT:
						// Adds 1 to the current cell value
						DataCellIncrement();
						break;
					case DATA_CELL_DECREMENT:
						// Subtracts 1 from the current cell value
						DataCellDecrement();
						break;
                    case TERMINATE:
						// Exit flag becomes true so that the program can be terminated
						Exit = true;
                        break;
					case LOOP_OPENING:
						// If the current cell is equal to 0, the CodeIndex jumps to the matching ']'
						if(Data[Pointer]==0)
							CodeIndex = FindMatching(CodeIndex);
						break;
					case LOOP_CLOSING:
						// If the current cell is not equal to 0, the CodeIndex jumps to the matching '['
						if(Data[Pointer]!=0)
							CodeIndex = FindMatching(CodeIndex);
						break;
					case ADD_CELL_RIGHT:
						// Adds a new cell to the Data list on the right
						Data.Add(0);
						break;
					case ADD_CELL_LEFT:
						// Adds a new cell to the Data list on the left
						Data.Reverse();
						Data.Add(0);
						Data.Reverse();
						// The pointer increments in order to point the same cell
						PointerIncrement();
						break;
					case OUTPUT_AS_NUMBER:
						// Outputs the current Data cell as a number
						Console.Write(Data[Pointer]);
						break;
					case OUTPUT_AS_UNICODE:
						// Outputs the current Data cell as an UNICODE character
						Console.Write((char)Data[Pointer]);
						break;
					case INPUT_SHOW:
						// Assigns the input character to the current Data cell and shows it
						Data[Pointer] = (byte)Console.ReadKey(false).KeyChar;
						break;
					case INPUT_HIDE:
						// Assings the input character to the current Data cell without showing it
						Data[Pointer] = (byte)Console.ReadKey(true).KeyChar;
						break;
                }
				// Goes to the next Code command
				CodeIndex++;
			}
		}
		
		/// <remarks>Increments the value at the current Data cell</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		protected void DataCellIncrement()
		{
			Data[Pointer]++;
		}
		
		/// <remarks>Decrements the value at the current Data cell</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		protected void DataCellDecrement()
		{
			Data[Pointer]--;
		}
		
		/// <remarks>Increments Data Pointer</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		protected void PointerIncrement()
		{
			Pointer++;
			// Wrapping behavior
			// Max Data index + 1 = 0
			if(Pointer>=Data.Count)
				PointerToStart();
		}
		
		/// <remarks>Decrements Data Pointer</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		protected void PointerDecrement()
		{
			Pointer--;
			// Wrapping behavior
			// 0 - 1 = max Data index
			if(Pointer<0)
				PointerToEnd();
		}
		
		/// <remarks>Moves Data Pointer to the beginning of the Data tape</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		protected void PointerToStart()
		{
			Pointer = 0;
		}
		
		/// <remarks>Moves Data Pointer to the end of the Data tape</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		protected void PointerToEnd()
		{
			Pointer = Data.Count - 1;
		}
		
		/// <remarks>Finds a symbol that matches the current one</remarks>
		/// <permission>protected so that potential child classes can call it directly</permission>
		/// <param name="index">Starting index which will be also used for iterating over the Code</param>
		/// <returns>Index of the matching symbol</returns>
		protected int FindMatching(int index)
		{
			// weight of Code symbols
			// If it's equal to 0 (balanced), it means that the matching symbol is found
			// Each non-matching symbol moves the weight further from the balance state
			// Each matching symbol moves the weight closer to the balance state
			int weight = 1;
			// Contains the character that the method is trying to match
			char toMatch = Code[index];
			// Tells if the search should go to the left (+1) or to the right (-1)
			int searchDirectionValue = SymbolSearchDirectionValues[toMatch];
			// The loop continues while the symbols weight is unbalanced, i.e. until the matching symbol is found
			while(weight!=0)
			{
				// The index moves to the defined direction
				index += searchDirectionValue;
				// Contains the current character that the method is checking
				char current = Code[index];
				// If it finds a non-matching symbol, the weight increments
				if(current==toMatch)
					weight++;
				// If the found symbol is a matching one, the weight decrements
				else if(current==MatchingSymbols[toMatch])
					weight--;
			}
			// Returns the index at which the symbols weight is balanced
			return index;
		}
	}

	/// <summary>A struct that represents an error symbol with its Character and information about its location in the source file</summary>
	/// <permission>Accessible only inside this namespace</permission>
	internal struct Symbol
	{
		/// <remarks>Number of the Code line in which the error occurs</remarks>
		/// <permission>public so that every method can read it</permission>
		/// <permission>readonly to be set only once</permission>
		public readonly uint Line;
		/// <remarks>Number of the Code column in which the error occurs</remarks>
		/// <permission>public so that every method can read it</permission>
		/// <permission>readonly to be set only once</permission>
		public readonly uint Column;
		/// <remarks>The character that causes the error</remarks>
		/// <permission>public so that every method can read it</permission>
		/// <permission>readonly to be set only once</permission>
		public readonly char Character;

		/// <remarks>Public constructor that takes the source file path and initializes each field</remarks>
		/// <permission>The constructor is public and can be called from everywhere</permission>
		/// <param name="line">Number of the line with error</param>
		/// <param name="column">Number of the column with error</param>
		/// <param name="character">The character that causes the error</param>
		public Symbol(uint line, uint column, char character)
		{
			Line = line;
			Column = column;
			Character = character;
		}
	}
}